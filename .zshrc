#
#    ______                                      _                 
#   |  ____|                                    | |                
#   | |__ _ __ ___  ___  ___ ___  _ __ _ __   __| | ___   __ _ ___ 
#   |  __| '__/ _ \/ _ \/ __/ _ \| '__| '_ \ / _` |/ _ \ / _` / __|
#   | |  | | |  __/  __/ (_| (_) | |  | | | | (_| | (_) | (_| \__ \
#   |_|  |_|  \___|\___|\___\___/|_|  |_| |_|\__,_|\___/ \__, |___/
#                                                         __/ |    
#                                                        |___/     
# 
#


set EDITOR "nvim"
unset TERM

# ssh EXPERIMENTAL
export SSH_KEY_PATH="~/.ssh/id_rsa"

# Colorful Man-Pages
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

# bat
alias cat='bat'

#-------- ALIASES --------#

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

# navigation
alias ..='cd ..' 
alias ...='cd ../..'

# vim
alias vim='nvim'

# Raspberry Pis
alias kalicon="ssh -p 2222 root@192.168.86.153"
alias picon='ssh -p 666 pi@192.168.86.76'

# Maintenance
alias update="sudo pacman-mirrors -g -f0 && paru -Syyu && sudo snap refresh"
alias cleanup="systemctl --failed && sudo update-grub && paru -c && paccache -rk1 && sudo pacman -Qtdq | sudo pacman -Rns -"

alias mirrors="sudo pacman-mirrors --interactive --default && sudo pacman -Syyu"
alias doh='commandline -i "sudo $history[1]"; history delete --exact --case-sensitive "$history[1]"'
alias brokensl='find ./ -path /media/* -prune -o -xtype l -print'
alias brokenrm='find . -xtype l -delete'

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# return external IP
alias xip='echo Your XXXternal IP is && curl -s https://icanhazip.com'

# quick launch
alias spot="ncspot"
alias cov='curl https://covid19.trackercli.com/USA'
alias worcov='curl https://covid19.trackercli.com'
alias phoneup='scrcpy -s 192.168.86.190:5555 -b2M -m800 -t'

# quick config
alias zup='vim .zshrc'
alias fup='vim ~/.config/fish/config.fish'
alias grubup='sudo update-grub'
alias tup='vim ~/.tmux.conf'
alias alup='vim .config/alacritty/alacritty.yml'
alias qup='vim ~/.config/qtile/config.py'
alias vup='vim .vimrc'
alias inup='vim ~/.config/nvim/init.vim'
alias starup='vim ~/.config/starship.toml'
alias paruup='vim ~/.config/paru/paru.conf'

alias clr='clear && source ~/.config/fish/config.fish'
alias c='clear && colorscript random'
alias svim='sudo nvim '
alias dots='yadm'
alias dotsall='yadm status && yadm add -u && yadm commit -m "auto message" && yadm push'

# shows if true color (Tc) is on in terminal
alias tctest='tmux info | grep Tc'

# This makes the "ip -a" command colorful
alias ip='ip -color=always'

# Minecraft
alias mineup="cd /home/freecorndogs/Minecraft_Server && java -Xmx2G -Xms512M -XX:SoftMaxHeapSize=1G -jar server.jar nogui"

# Random Color Script
colorscript random

#Runs Starship Prompt
eval "$(starship init zsh)"
