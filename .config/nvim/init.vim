"
"    ______                                      _                 
"   |  ____|                                    | |                
"   | |__ _ __ ___  ___  ___ ___  _ __ _ __   __| | ___   __ _ ___ 
"   |  __| '__/ _ \/ _ \/ __/ _ \| '__| '_ \ / _` |/ _ \ / _` / __|
"   | |  | | |  __/  __/ (_| (_) | |  | | | | (_| | (_) | (_| \__ \
"   |_|  |_|  \___|\___|\___\___/|_|  |_| |_|\__,_|\___/ \__, |___/
"                                                         __/ |    
"                                                        |___/     
"

" this sets the default file to the .vimrc file

set termguicolors
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath
source ~/.vimrc
