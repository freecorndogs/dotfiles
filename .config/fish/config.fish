#
#    ______                                      _                 
#   |  ____|                                    | |                
#   | |__ _ __ ___  ___  ___ ___  _ __ _ __   __| | ___   __ _ ___ 
#   |  __| '__/ _ \/ _ \/ __/ _ \| '__| '_ \ / _` |/ _ \ / _` / __|
#   | |  | | |  __/  __/ (_| (_) | |  | | | | (_| | (_) | (_| \__ \
#   |_|  |_|  \___|\___|\___\___/|_|  |_| |_|\__,_|\___/ \__, |___/
#                                                         __/ |    
#                                                        |___/     
#


# Editor Configuration
set -gx EDITOR 'nvim'
set -gx VISUAL 'nvim'
set -gx SUDO_EDITOR 'nvim'

# Disable Fish Greeting
set fish_greeting

# Terminal Configuration
tmux set-option -g default-terminal "alacritty"

# Set terminal type
#set -gx TERM "screen-256color"
#export TERM="screen-256color"
export TERM="alacritty"
set -gx TERM alacritty

# Language and Shell Configuration
set -gx LANG "en_US.UTF-8"
set -gx SHELL /usr/bin/fish

# Colorful Man-Pages
set -Ux MANPAGER "sh -c 'col -bx | bat -l man -p'"

# Use bat as a replacement for `cat`
alias cat='bat'

# Load SSH Agent
set -x SSH_AUTH_SOCK $XDG_RUNTIME_DIR/ssh-agent.socket

# Start the SSH agent if not already running
if not pgrep ssh-agent > /dev/null
    eval (ssh-agent -c)
end

# Check if keys are loaded, and load them if necessary
if test -z (ssh-add -l | grep "da_key")
    ssh-add ~/.ssh/da_key
end




# Beauty and Color Configuration

# Apply random color script
colorscript -e pacman

# Add colors to grep output
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Make `ip` command colorful
alias ip='ip -color=always'

# Set color for fish auto-complete and highlights
set -g fish_color_normal brcyan
set -g fish_color_autosuggestion '#7d7d7d'
set -g fish_color_command brcyan
set -g fish_color_error '#ff6c6b'
set -g fish_color_param brcyan

# Beautify `ls` command
alias ls='exa -alh --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias dirsize='ncdu'

# Make `mount` output pretty
alias mount='mount | column -t'

# Aliases

# Confirm before overwriting
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias rmdir='rm -rf -i'

# Navigation
alias ..='cd ..'
alias ...='cd ../..'

# Replace `vim` with `nvim`
alias vim='nvim'

# Package Management
alias update='paru -Syu --removemake --sudoloop && arch-audit -u && flatpak update'
alias install='paru -S --removemake --sudoloop'
alias remove='paru -Rs'
alias cleanup='sudo pacman -Sc --noconfirm && sudo pacman -Scc --noconfirm && sudo journalctl --vacuum-size=10M && sudo pacman -Qdtq | xargs -r sudo pacman -Rn --noconfirm && sync && flatpak uninstall --unused'
alias mirrors="sudo reflector-simple"
alias doh='commandline -i "sudo $history[1]"; history delete --exact --case-sensitive "$history[1]"'
alias brokensl='find ./ -path /media/* -prune -o -xtype l -print'
alias brokenrm='find . -xtype l -delete'
alias pacnew='find /etc -regextype posix-extended -regex ".+\.pac(new|save)" 2> /dev/null'

# Dotfiles
#alias dots='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias dots='yadm'
# get error messages from journalctl
alias jfail="journalctl -p 3 -xb"

# List enabled systemctl services
alias enabled="systemctl list-unit-files | grep enabled"
alias running="systemctl | grep running"

# Return external IP
alias xip='echo Your XXXternal IP is && curl -s https://icanhazip.com'

# quick config
alias zup='vim .zshrc'
alias fup='vim ~/.config/fish/config.fish'
alias tup='vim ~/.tmux.conf'
alias alup='vim .config/alacritty/alacritty.yml'
alias vup='vim .vimrc'
alias startup='vim /home/freecorndogs/startpage/startpage.html'
alias inup='vim ~/.config/nvim/init.vim'
alias starup='vim ~/.config/starship.toml'
alias paruup='vim ~/.config/paru/paru.conf'
alias pacup='sudo vim /etc/pacman.conf'
alias clr='clear && source ~/.config/fish/config.fish'
alias c='clear && colorscript -e pacman'
alias svim='sudo nvim '

# Shows if true color (Tc) is working in terminal
alias tctest='tmux info | grep Tc'
alias tctest2='printf "\x1b[38;2;255;100;0mTRUECOLOR\x1b[0m\n"'

# Teamviewer service
alias tvup='sudo systemctl enable teamviewerd && teamviewer --daemon start'
alias tvdown='sudo systemctl disable teamviewerd && teamviewer --daemon stop'

# Networking
alias ports='netstat -tulanp'
# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
# Do not wait interval 1 second, go fast #
alias fastping='ping -c 100 -s.2'
alias mynetwork "echo External IP:; curl ifconfig.co; echo; echo Internal IP:; ip addr show | grep 'inet ' | awk '{print \$2}'; echo; echo DNS:; cat /etc/resolv.conf | grep nameserver | awk '{print \$2}'; echo; echo Gateway IP:; ip route | grep default | awk '{print \$3}'"

# Starship shell prompt (this has to be at the end)
starship init fish | source
