"
"    ______                                      _
"   |  ____|                                    | |
"   | |__ _ __ ___  ___  ___ ___  _ __ _ __   __| | ___   __ _ ___
"   |  __| '__/ _ \/ _ \/ __/ _ \| '__| '_ \ / _` |/ _ \ / _` / __|
"   | |  | | |  __/  __/ (_| (_) | |  | | | | (_| | (_) | (_| \__ \
"   |_|  |_|  \___|\___|\___\___/|_|  |_| |_|\__,_|\___/ \__, |___/
"                                                         __/ |
"                                                        |___/
"


" Vundle Plugin Manager for Vim

" Set nocompatible for improved features
set nocompatible

" Turn off filetype detection
filetype off

" Start Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"{{ Basics }}
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim' " Lightline status bar

"{{ File management }}
Plugin 'ryanoasis/vim-devicons' " Icons for NerdTree

"{{ Productivity }}
Plugin 'tpope/vim-surround' " Change surrounding marks

"{{ Syntax highlighting and colors }}
Plugin 'vim-python/python-syntax' " Python syntax highlighting
Plugin 'ap/vim-css-color' " Color previews for CSS
Plugin 'leafgarland/typescript-vim'
Plugin 'scrooloose/syntastic'

"{{ Junegunn Choi plugins }}

call vundle#end()

" Turn on filetype detection and plugin indent
filetype plugin indent on

" Brief help
" :PluginList - lists configured plugins
" :PluginInstall - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean - confirms removal of unused plugins; append `!` to auto-approve removal

" See :h vundle for more details or the wiki for FAQ

" Settings
set path+=** " Searches the current directory recursively
set wildmenu " Displays all matches when tab completing
set incsearch " Incremental search
set hidden " Needed to keep multiple buffers open
set nobackup " No automatic backups
set noswapfile " No swap file
set t_Co=256 " Set if the terminal supports 256 colors
set number " Display line numbers
set clipboard=unnamedplus " Copy/paste between Vim and other programs
syntax enable
let g:rehash256 = 1
let g:python3_host_prog = '/usr/bin/python3'

" Special insert function
" Remap ESC to ii
:imap ii <Esc>

" Exit insert mode when the window loses focus
autocmd FocusLost * call feedkeys("\<esc>")

" Lightline status bar theme
let g:lightline = {
  \ 'colorscheme': 'wombat',
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ],
  \     [ 'filename', 'fileformat', 'fileencoding', 'filetype' ] ],
  \   'right': [ [ 'lineinfo' ],
  \     [ 'percent' ],
  \     [ 'fileformat', 'fileencoding', 'filetype' ] ]
  \ }
  \}
